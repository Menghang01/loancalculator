import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    originalPrice: 0,
    initial: 0,
    interest: 0, 
    years: 0, 
  },
  mutations: {
    set_originalPrice(state, originalPrice) {
      state.originalPrice = originalPrice
    },
    set_initial(state, initial) {
      state.initial = initial
    },
    set_interest(state, interest){
      state.interest = interest
    },
    set_years(state, years){
      state.years = years
    }
  },

  getters: {
    getPrice(state){
      let num = state.originalPrice - state.initial
       return Math.round( (num + Number.EPSILON) * 100) / 100
      
    }, 

    getMonth(state){
       return state.years * 12 
    }, 

    getAll(state,getters){
      // let num = getters.getPrice * Math.pow((1 + state.interest / (100 * 12)),getters.getMonth)
      let num = getters.getPerMonth * getters.getMonth
      return Math.round( (num + Number.EPSILON) * 100) / 100
      

    },

    getPerMonth(state,getters){
      // let a = getters.getPrice * Math.pow((1 + state.interest / (100 * 12)),getters.getMonth)
      let interestpm = (state.interest/100) / 12
      let num = (getters.getPrice * interestpm * Math.pow((1 + interestpm), getters.getMonth)) / (Math.pow((1 + interestpm), getters.getMonth) - 1);
      // let num = a / getters.getMonth 
      if (isNaN(num)) {
        return 0
      }else {
        return  Math.round( (num + Number.EPSILON) * 100) / 100 
      }

    },

    getInterest(state,getters){
      // let num = (state.originalPrice - state.initial) * Math.pow((1 + state.interest / (100 * getters.getMonth)),getters.getMonth) - getters.getPrice
      let num = getters.getAll - getters.getPrice
       return Math.round( (num + Number.EPSILON) * 100) / 100
      

    }

  },
  actions: {
    set_originalPrice({ commit }, originalPrice){
      commit('set_originalPrice', originalPrice)
    },
    set_initial({ commit }, initial){
      commit('set_initial', initial)
    },
    set_interest({ commit }, interest){
      commit('set_interest', interest)
    },
    set_years({ commit }, years){
      commit('set_years', years)
    }
  },
  modules: {
  }
})
